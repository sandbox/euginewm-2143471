<?php

/**
 * @file
 * ping node module ping_node.admin.inc
 *
 * Development module for Drupal
 * which help us manager ping services
 */


/**
 * Default list of configuration
 */
function ping_node_default_setings() {
  $services = array(
    'http://ping.blogs.yandex.ru/RPC2',
    'http://blogsearch.google.com/ping/RPC2',
    'http://rpc.pingomatic.com',
    'http://xping.pubsub.com/ping/',
    'http://rpc.weblogs.com/RPC2',
    'http://blogpeople.net/servlet/weblogUpdates',
  );

  $services = implode("\n", $services);

  return $services;
}

/**
 * MENU CALLBACK
 *
 * @see ping_node_menu()
 */
function ping_node_administer() {
  $form = array();

  $services = ping_node_default_setings();

  $form['ping_node_servises'] = array(
    '#type' => 'textarea',
    '#description' => t('put element per line'),
    '#title' => t('Servises url list'),
    '#default_value' => variable_get('ping_node_servises', $services),
    '#rows' => 6,
    '#required' => TRUE,
  );

  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset all setings to default'),
    '#weight' => 19,
  );

  return system_settings_form($form);
}

/**
 *  Validating  ping_node_administer Form for Correct url
 *
 */
function ping_node_administer_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Reset all setings to default')) {
    $services = ping_node_default_setings();
    $form_state['values']['ping_node_servises'] = $services;
    return;
  }

  $services = $form_state['values']['ping_node_servises'];
  $r = preg_match('/\r?/', $services) ? "\r" : '';
  $services = explode("$r\n", $services);

  foreach ($services as $url) {
    if (!valid_url($url, TRUE)) {
      form_set_error('ping_node_servises',
                      t('url -> @url Not correct! do something!',
                        array('@url' => $url)));
    }
  }
}
